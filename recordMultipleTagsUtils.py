
def convertToInt(mantissa_str):
    power_count = -1
    mantissa_int = 0
    for i in mantissa_str:
        mantissa_int += (int(i) * pow(2, power_count))
        power_count -= 1
    return (mantissa_int + 1)


def bin_to_float(input):
    result = int(input[1:9], 2)
    exponent_unbias = result - 127
    sign_bit = int(input[0])
    mantissa_int = convertToInt(input[9:])
    real_no = pow(-1, sign_bit) * mantissa_int * pow(2, exponent_unbias)
    return(real_no)


def four_byte_convert(input):
    newString = input[6:8] + input[4:6] + input[2:4] + input[0:2]
    return(bin_to_float(format(int(newString, 16), "032b")))


def five_byte_convert(input):
    newString = input[8:10] + input[6:8] + input[4:6] + input[2:4] + input[0:2]
    return(int(newString, 16))


def moving_average(x, w):
    return np.average(x[len(x)-w:len(x)])


def createRowOfData(id, time, hexString):
    range = four_byte_convert(hexString[6:14])
    rss = four_byte_convert(hexString[16:24])
    PollRecieved = five_byte_convert(hexString[26:36])
    PollAckSent = five_byte_convert(hexString[38:48])
    RangeRecieved = five_byte_convert(hexString[50:60])
    PollSent = five_byte_convert(hexString[62:72])
    PollAckRecieved = five_byte_convert(hexString[74:84])
    RangeSent = five_byte_convert(hexString[86:96])
    return [id, time, range, rss, PollRecieved, PollAckSent, RangeRecieved,
            PollSent, PollAckRecieved, RangeSent]
