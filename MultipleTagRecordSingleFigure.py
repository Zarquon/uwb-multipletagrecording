import openpyxl
from datetime import datetime
import time
import recordMultipleTagsUtils
import serial
import matplotlib.pyplot as plt
import threading


MaxPlotDistance = 50  # Largest value plotted on the x axis
liveGraphQuantity = 250  # The ammount of points displayed
portName = '/dev/tty.wchusbserial14310'  # Serial port anchor is connceted to
print("Connecting to Serial Port :", portName)
serialPort = serial.Serial(port=portName, baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)


def signal_user_input():
    global no_input
    input("exit")
    no_input = False


threading.Thread(target=signal_user_input).start()


y = []
x = []
no_input = True
idsOfAnchorsConnected = []
lastPlot = time.time() * 1000
startTimeMilli = time.time() * 1000
startTime = datetime.now()
lastHexString = '00'
plt.ion()
book = openpyxl.Workbook()
book.save(filename=str(startTime) + '.xlsx')

print("-----------------------------------------------")
print("Starting Program")
print("To stop the progam,type 'exit' into terminal then press enter ")
print("Data will be saved into file: ", str(startTime) + '.xlsx')

while no_input:
    try:
        hexStringStart = serialPort.read(1).hex()
    except:
        lastHexString = '00'
        hexStringStart = '00'
        print("-----------DISCONNECTED---------------")
        try:
            serialPort = serial.Serial(port=portName, baudrate=115200,
                                   bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)
            print("#############RECONNECTED################")
        except:
            print("failed to reconnect trying again in 1 seconds")
            time.sleep(1)


    if lastHexString + hexStringStart == 'ffff':
        var = serialPort.read(58)
        hexString = var.hex()

        id = hexString[0:4]

        if id not in idsOfAnchorsConnected:
            book.create_sheet(str(id))
            idsOfAnchorsConnected.append(id)
            Row = ['id', 'time (unix)', 'range (m)', 'rss', 'PollRecieved',
                         'PollAckSent', 'RangeRecieved',
                         'PollSent', 'PollAckRecieved', 'RangeSent']
            sheet = book[str(id)]
            sheet.append(Row)

        sheet = book[str(id)]
        Row = recordMultipleTagsUtils.createRowOfData(id, int(time.time()*1000), hexString)
        sheet.append(Row)
        book.save(filename=str(startTime) + '.xlsx')
        lastHexString = '00'

    else:
        lastHexString = hexStringStart

    if (time.time()*1000 - lastPlot >= 500):
        plt.figure(1)
        plt.clf()
        plt.axis([-60, 1, 0, MaxPlotDistance])
        plt.xlabel("Last minute of recorded measurements")
        plt.ylabel("Distance (m)")

        lastPlot = time.time()*1000

        for id in idsOfAnchorsConnected:
            sheet = book[str(id)]
            y = []
            for row in sheet.iter_rows(min_row=sheet.max_row - liveGraphQuantity + 1
                                    if sheet.max_row > liveGraphQuantity
                                    else 2, min_col=3, max_col=3, max_row=sheet.max_row, values_only=True):
                y.append(row[0])

            x = []
            for row in sheet.iter_rows(min_row=sheet.max_row - liveGraphQuantity + 1
                                    if sheet.max_row > liveGraphQuantity
                                    else 2, min_col=2, max_col=2, max_row=sheet.max_row, values_only=True):
                x.append(-(time.time()*1000 - row[0])/1000)

            line, = plt.plot(x, y)
            line.set_label(id)
            plt.legend(loc='upper left')

        plt.draw
        plt.pause(0.01)

print("exiting program")
print("Saving sheet please don't close the program")
book.save(filename=str(startTime) + '.xlsx')
