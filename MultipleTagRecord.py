import openpyxl
from datetime import datetime
import time
import recordMultipleTagsUtils
import serial
import matplotlib.pyplot as plt


liveGraphQuantity = 50

x = list(range(0, liveGraphQuantity))

plt.ion()

startTime = datetime.now()
print("Program starting at :", startTime)


portName = '/dev/tty.wchusbserial14110'
print("Connecting to Serial Port :", portName)
serialPort = serial.Serial(port=portName, baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)

book = openpyxl.Workbook()
book.save(filename=str(startTime) + '.xlsx')

idsOfAnchorsConnected = []

lastHexString = '00'
try:
    while True:

        hexStringStart = serialPort.read(1).hex()

        if lastHexString + hexStringStart == 'ffff':

            var = serialPort.read(58)
            hexString = var.hex()

            id = hexString[0:4]

            if id not in idsOfAnchorsConnected:
                book.create_sheet(str(id))
                idsOfAnchorsConnected.append(id)
                Row = ['id', 'time (unix)', 'range (m)', 'rss', 'PollRecieved',
                       'PollAckSent', 'RangeRecieved',
                       'PollSent', 'PollAckRecieved', 'RangeSent']
                sheet = book[str(id)]
                sheet.append(Row)
                plt.figure(id)
                plt.title(str(id))
                plt.xlabel("Last 50 measurements recorded")
                plt.ylabel("Distance (m)")

            sheet = book[str(id)]
            Row = recordMultipleTagsUtils.createRowOfData(id, str(time.time()),
                                                          hexString)
            sheet.append(Row)
            lastHexString = '00'
            book.save(filename=str(startTime) + '.xlsx')

            y = []
            for row in sheet.iter_rows(min_row=sheet.max_row - liveGraphQuantity + 1
                                       if sheet.max_row > liveGraphQuantity
                                       else 2, min_col=3, max_col=3, max_row=sheet.max_row, values_only=True):
                y.append(row)

            plt.figure(id)
            plt.cla()
            plt.title(str(id))
            plt.xlabel("Last 50 measurements recorded")
            plt.ylabel("Distance (m)")
            plt.plot(x[0: len(y)], y)
            plt.pause(0.01)

        else:
            lastHexString = hexStringStart


except KeyboardInterrupt:
    print("exiting program")
    print("Saving sheet please don't close the program")
    book.save(filename=str(startTime) + '.xlsx')
