# UWB MultipleTagRecording

Program to live graph and record multiple tags

# Mac running
python3 MultipleTagRecordSingleFigure.py

#exiting the program
To exit the program type 'exit' into the terminal and press enter

# Changes 
Line 10: MaxPlotDistance determines the max range of the y axis, Preset to 50 meters
Line 12: Port name the UWB anchor is attached to
